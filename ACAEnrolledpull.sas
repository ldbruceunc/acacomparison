**********************************************************
Goal is to pull claims for BCBSNC ACA enrolled 2014-2019 as a comparator group
for Medicaid
Requestor: Justin Trogdon

Programmer: LDBruce
Last update: 15MAR2021
**********************************************************
;

libname raw "/nearline/data/master/BCBS_SVC_Master";
libname outdata "/local/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ACA/data";
libname nearline "/nearline/data/projects/BCBS/Data/"; 


%let run_dt = %sysfunc(putn(%sysfunc(date()),MMDDYY10.));
%let xref = raw.member_bcbsnc_xref_202007;
%let aca19 = raw.sheps_aca_2019;
%let aca18 = raw.sheps_aca_2018;
%let aca17 = raw.sheps_aca_2017;
%let acayrs = raw.sheps_aca_2010_2016;

data num_id;
	set &xref;
	POL_MBR_ID = POLICY_MEMBER_ID * 1;
run;

proc format;
value $metal
	'Cata','Catast' = '1_Catastrophic'
	'Bron','Bronze' = '2_Bronze'	
	'Silv','Silver' = '3_Silver'
	'Gold' = '4_Gold'
	'Plat','Platin' = '5_Platinum';
	run;



ods html5 path="/local/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ACA" file="ACA_explor.html";
*xref file for Unique ID;
title 'Crosswalk file';
proc print data= &xref (obs=5);
run;

proc contents data= &xref;
run;


*ACA files by year - values change;
%macro mnames(yr, yrdat);
title "ACA file &yr";
proc freq data=&yrdat;
tables MTLC_TYPE_DESC;
where year(cvr_mnth_dt) = &yr;
run;
%mend mnames;
%mnames(2019,&aca19);
%mnames(2018,&aca18);
%mnames(2017,&aca17);
%mnames(2016,&acayrs);
%mnames(2015,&acayrs);
%mnames(2014,&acayrs);

title 'ACA print and contents 2017';
proc print data= &aca17 (obs=5);
run;
proc contents data= &aca17;
run;
ods html5 close;


*Unique member months and plan type;
ods html5 path="/local/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ACA" file="ACA_output.html";

proc sql ;
create table umi as 
select distinct cvr_mnth_dt as EnrollMonth , MTLC_TYPE_DESC format = $metal., count(distinct(unique_member_id)) as UMID
from &aca19 a
left join num_id b on a.POL_MBR_ID = b.POL_MBR_ID
group by 1,2

union all

select distinct cvr_mnth_dt as EnrollMonth , MTLC_TYPE_DESC format = $metal., count(distinct(unique_member_id)) as UMID
from &aca18 a
left join num_id b on a.POL_MBR_ID = b.POL_MBR_ID
group by 1,2

union all

select distinct cvr_mnth_dt as EnrollMonth , MTLC_TYPE_DESC format = $metal., count(distinct(unique_member_id)) as UMID
from &aca17 a
left join num_id b on a.POL_MBR_ID = b.POL_MBR_ID
group by 1,2

union all

select distinct cvr_mnth_dt as EnrollMonth , MTLC_TYPE_DESC format = $metal., count(distinct(unique_member_id)) as UMID
from &acayrs a
left join num_id b on a.POL_MBR_ID = b.POL_MBR_ID
group by 1,2

order by EnrollMonth
;
quit;

proc sql;
create table umi2 as
select EnrollMonth format = MONYY7., MTLC_TYPE_DESC as MetalPlan format = $metal., UMID
from umi
where MTLC_TYPE_DESC not in ('a Mu')
order by EnrollMonth, put(MetalPlan, $metal.)
;
quit;


proc transpose data=umi2 out=umi3 (drop=_name_);
by EnrollMonth;
ID MetalPlan;
run;
ods excel file="/local/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ACA/ACAMemMonths.xlsx"
	options(autofilter='all' sheet_name='memmonths');
title "/n2/users/liana/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ACA/ACAEnrolledpull.sas";
title2 "Last update: &run_dt.";
proc print data=umi3 noobs;
var EnrollMonth '1_Catastrophic'n '2_Bronze'n '3_Silver'n '4_Gold'n '5_Platinum'n;
format _numeric_ comma12. EnrollMonth MONYY7.;
run;

ods excel close;
ods html5 close;



/*

proc print data= &aca17 (obs=5);
run;

proc contents data= &aca17;
run;

ods html5 path="/local/projects/Medicaid_Waiver_Eval/analytics/users/lbruce/ACA" file="ACA_plantype.html";
title 'Metal type';
proc sql ;
create table umi as
select distinct year(cvr_mnth_dt) as Year, MTLC_TYPE_DESC, count(distinct(unique_member_id)) as UMID
from &aca19 a
left join num_id b
on a.POL_MBR_ID = b.POL_MBR_ID
group by 1,2
; 
quit;

proc print data=umi;
format UMID comma12.;
run;
ods html5 close;



proc sql ;
create table umi4 as
select distinct cvr_mnth_dt,  count(distinct(unique_member_id)) as UMID
from &aca19 a
left join num_id b
on a.POL_MBR_ID = b.POL_MBR_ID
group by 1
order by 1
;
quit;

proc sql ;
create table umi5 as
select mean(UMID) as AvgPersonMonth2019
from umi4
;
quit;

proc print data=umi5;
run;




/*
proc sql ;
create table umi2 as
select distinct cvr_mnth_dt,  
count(POL_MBR_ID) as polmemID
from &aca19
group by 1
order by 1
;
quit;

proc sql ;
create table umi3 as
select mean(polmemID)
from umi2
;
quit;

proc print data=umi3;
run;






/*













